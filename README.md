## Privacy Policy

The TubeHack project aims to provide a private, anonymous experience for using media web services.
Therefore, the app does not collect any data without your consent. TubeHack's privacy policy explains in detail what data is sent and stored when you send a crash report.

## License
[![GNU GPLv3 Image](https://www.gnu.org/graphics/gplv3-127x51.png)](http://www.gnu.org/licenses/gpl-3.0.en.html)  

TubeHack is Free Software: You can use, study share and improve it at your
will. Specifically you can redistribute and/or modify it under the terms of the
[GNU General Public License](https://www.gnu.org/licenses/gpl.html) as
published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.  
